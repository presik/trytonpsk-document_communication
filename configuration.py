# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Id


class Configuration(metaclass=PoolMeta):
    __name__ = 'document.configuration'
    communication_incoming_sequence = fields.Many2One('ir.sequence',
        'Communication Incoming Sequence', required=True, domain=[
            ('sequence_type', '=',
                Id('document_communication', 'sequence_type_document_communication'))])
    communication_outgoing_sequence = fields.Many2One('ir.sequence',
        'Communication Outgoing Sequence', required=True, domain=[
            ('sequence_type', '=',
                Id('document_communication', 'sequence_type_document_communication'))])
    template_communication = fields.Many2One('email.template',
        'Template Communication')
    dir_comm = fields.Char('Directory Commnications')
