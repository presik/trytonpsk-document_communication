# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
from datetime import datetime, date, time
import subprocess

from io import BufferedReader, BytesIO
from dateutil import tz
try:
    from PyPDF2 import PdfFileWriter, PdfFileReader
    from reportlab.pdfgen import canvas
    from reportlab.lib.pagesizes import letter
    from reportlab.lib.colors import HexColor
except Exception as e:
    print("Error: module is missing: ", e)
    print("Please install it with: $pip3 install pypdf2")
    print("Please install it with: $pip3 install ReportLab")

from trytond import backend
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.model import ModelView, ModelSQL, Workflow, fields
from trytond.pyson import Eval, And, In, Not, Bool, Or
from trytond.modules.company import CompanyReport
from trytond.wizard import Wizard, StateTransition, StateView, Button, StateReport
from trytond.transaction import Transaction
from trytond.config import config
from trytond.report import Report
from trytond.i18n import gettext
from trytond.model.exceptions import AccessError


_TYPE = [
        ('incoming', 'Incoming'),
        ('outgoing', 'Outgoing'),
        ('response', 'Response'),
        ('internal', 'Internal'),
]

INVERT_TYPE = {
    'incoming': 'outgoing',
    'outgoing': 'incoming',
}

_STATES = {
    'readonly': Eval('state') != 'draft',
}

PRIORITY = [
        ('low', 'Low'),
        ('middle', 'Middle'),
        ('high', 'High'),
]


from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Bogota')

# FIXME set user in context as domian for states
# 'invisible': And(
#     Not(In(Eval('_user', '0'), Eval('users_target', []))),
#     Eval('create_uid') != Eval('_user', '0'),
# )


class Communication(Workflow, ModelSQL, ModelView):
    'Communication'
    __name__ = 'document.communication'
    _rec_name = 'number'
    number = fields.Char("Number", readonly=True, select=True)
    reference = fields.Char("Reference", states=_STATES, select=True)
    description = fields.Char('Description', required=True, states=_STATES)
    service_order = fields.Char('Service Order', states=_STATES)
    party = fields.Many2One('party.party', 'Party',
        states=_STATES)
    users_target = fields.Many2Many('res.user-document.communication',
        'document_communication', 'user', 'User Target',
        states=_STATES, required=True)
    date_communication = fields.DateTime('Date', states=_STATES)
    comment = fields.Text('Comment', states={
            'invisible': Eval('state') not in ['filing', 'checked'],
            'readonly': Eval('state') == 'checked',
        })
    date_checked = fields.Date('Date Checked', states={
            'readonly': True,
            })
    kind = fields.Many2One('document.communication.kind',
        'Communication Kind', required=True, states={
            'readonly': Bool(Eval('number')),
        }, domain=[('type', '=', Eval('type_communication'))],
        depends=['type_communication'],
    )
    receive_original = fields.Many2One('company.employee',
        'Receive Original', states={
            'required': Eval('state') != 'draft',
            'readonly': Eval('state') != 'draft',
        })
    type_communication = fields.Selection(_TYPE, 'Type', states={
            'readonly': True,
            }, select=True)
    tag_location = fields.Selection([
        ('left', 'Left'),
        ('right', 'Right'),
        ('center', 'Center'),
        ('below', 'Below'),
        ('middle', 'Middle A'),
        ('middle_b', 'Middle B'),
        ], 'Tag Location', select=True, required=True, states=_STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('signed', 'Signed'),
        ('filing', 'Filing'),
        ('wait_response', 'Wait Response'),
        ('checked', 'Checked'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled'),
        ], 'State', readonly=True, select=True)
    state_string = state.translated('state')
    priority = fields.Selection(PRIORITY, 'Priority', required=True, select=True, states={
            'readonly': Eval('state').in_(['done', 'cancelled']),
        })
    priority_string = priority.translated('priority')
    attachments = fields.One2Many('document.communication.attachment', 'communication',
            'Attachments', states={
                'readonly': Eval('state') != 'signed'
            }
        )
    company = fields.Many2One('company.company', 'Company',
            states=_STATES, required=True)
    comm_group = fields.Many2One('document.communication.group', 'Group',
            states=_STATES)
    require_res = fields.Boolean('Require Response', states={
                'invisible': Eval('state').in_(['draft', 'signed']),
                'readonly': Eval('state').in_(['done', 'cancelled']),
                })
    date_response = fields.Date('Date Response', states={
                'invisible': Eval('state').in_(['draft', 'signed']),
                'readonly': Eval('state') == 'done',
                'required': Bool(Eval('require_res')),
                })
    days_response = fields.Function(fields.Integer('Days Response',
            states={
                'invisible': Eval('state') == 'done',
                }), 'get_days_response')
    checked_by = fields.Many2One('res.user', 'Checked By',
            states={
                'readonly': True,
                })
    responsible = fields.Many2One('res.user', 'Reponsible',
            states={
                'invisible': Eval('state') == 'draft',
                'readonly': Eval('state') == 'done',
                'required': Eval('state') == 'checked',
            })
    responsible2 = fields.Many2One('res.user', 'Other Reponsible',
            states={
                'invisible': Eval('state') == 'draft',
                'readonly': Eval('state') == 'done',
            })
    appends = fields.Char('Appends', states=_STATES)
    mail_sended = fields.Boolean('Mail Sended', readonly=True)
    response = fields.Many2One('document.communication',
            'Response', states={
                'required': And(
                    Eval('state') == 'done', Bool(Eval('require_res'))
                ),
                'readonly': Eval('state') == 'done',
            }, depends=['state', 'party'], domain=[
                ('type_communication', '!=', Eval('type_communication')),
                ('party', '=', Eval('party')),
                ('state', 'not in', ['draft', 'cancelled']),
            ])
    date_communication_tz = fields.Function(fields.Char('Date Tz'),
            'get_date_communication_tz')

    @classmethod
    def __setup__(cls):
        super(Communication, cls).__setup__()
        cls._order.insert(0, ('date_communication', 'DESC'))
        cls._transitions |= set((
                ('draft', 'cancelled'),
                ('draft', 'signed'),
                ('signed', 'filing'),
                ('signed', 'draft'),
                ('filing', 'checked'),
                ('checked', 'wait_response'),
                ('checked', 'done'),
                ('wait_response', 'done'),
                ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'signed',
                },
            'checked': {
                'invisible': Eval('state') != 'filing',
                },
            'filing': {
                'invisible': Eval('state') != 'signed',
                },
            'signed': {
                'invisible': Eval('state') != 'draft',
                },
            'done': {
                'invisible': ~Eval('state').in_(['checked', 'wait_response']),
                },
            'wait_response': {
                'invisible': Eval('state') != 'checked',
                },
            'send_mail': {
                'invisible': (Eval('mail_sended')) | (Eval('state') != 'filing'),
                },
            'respond': {
                'invisible': Or(
                        Eval('state').in_(['draft', 'signed', 'filing']),
                        Eval('type_communication') == 'outgoing'
                ),
            },
        })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_tag_location():
        return 'middle'

    @staticmethod
    def default_date_communication():
        return datetime.now()

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_priority():
        return 'middle'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, docs):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('signed')
    def signed(cls, docs):
        for doc in docs:
            doc.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('checked')
    def checked(cls, docs):
        user_id = Transaction().user
        for doc in docs:
            doc.write([doc], {
                'date_checked': date.today(),
                'checked_by': user_id,
            })

    @classmethod
    @ModelView.button
    @Workflow.transition('filing')
    def filing(cls, docs):
        if not docs:
            raise AccessError(
                gettext('document_communication.msg_missing_attachments')
            )
        for doc in docs:
            doc.update_attachments_name()

    @classmethod
    @ModelView.button
    @Workflow.transition('wait_response')
    def wait_response(cls, docs):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, docs):
        pass

    @classmethod
    @ModelView.button_action('document_communication.act_open_communication_letter_form')
    def respond(cls, docs):
        for doc in docs:
            doc._create_response()

    @classmethod
    @ModelView.button
    def send_mail(cls, docs):
        for doc in docs:
            cls._send_mail(doc)

    def _create_response(self):
        pool = Pool()
        Letter = pool.get('document.communication.letter')
        Kind = pool.get('document.communication.kind')
        kinds = Kind.search([
            ('default_party', '=', self.party.id),
            ('type', '=', 'outgoing'),
        ])
        kind = None
        if kinds:
            kind = kinds[0]
        else:
            kinds = Kind.search([
                ('default_party', '=', None),
                ('type', '=', 'outgoing'),
            ])
            if kinds:
                kind = kinds[0]
        if not kind:
            return

        letter, = Letter.create([{
            'date': date.today(),
            'subject': self.reference or '' + ' - ' + self.description,
            'company': self.company.id,
            'party': self.party.id,
            'comm_group': self.comm_group.id,
            'kind': kind.id,
            'priority': self.priority,
            'type': 'legal',
            'communication_incoming': self.id,
        }])
        letter.set_number()

    @classmethod
    def _send_mail(cls, doc):
        pool = Pool()
        Config = pool.get('document.configuration')
        config = Config.get_configuration()
        Template = pool.get('email.template')
        if not config.template_communication:
            raise AccessError(
                gettext('document_communication.msg_missing_template_communication')
            )
        else:
            context = Transaction().context.copy()
            with Transaction().set_context(context):
                template = config.template_communication
                template.subject = template.subject + f'{ doc.kind.name }: {doc.description }'
                Template.send(template, doc, doc.get_mail_users_target())
                # Template.render_and_send(
                #     config.template_communication,
                #     [doc],
                # )
            cls.write([doc], {'mail_sended': True})

    def get_days_response(self, name=None):
        if self.date_response and self.state not in ['cancelled', 'done']:
            return (self.date_response - date.today()).days

    def set_number(self):
        if self.number:
            return
        Sequence = Pool().get('ir.sequence')
        if not self.kind or not self.kind.sequence:
            raise AccessError(
                gettext('document_communication.msg_sequence_missing')
            )
        seq = self.kind.sequence.get()
        self.write([self], {'number': seq})

    def update_attachments_name(self):
        Config = Pool().get('document.configuration')
        config = Config(1)
        path_ftp = config.path_ftp
        if not path_ftp:
            raise AccessError(
                gettext('document_communication.msg_missing_path_ftp')
            )
        for attachment in self.attachments:
            if attachment.name[-4:] != '.pdf':
                raise AccessError(
                    gettext('document_communication.msg_invalid_pdf_name')
                )
                return
            name_ = attachment.name[:-4]
            normalize_name = name_.upper() + '.pdf'
            new_name = attachment.communication.number + '_' + normalize_name
            old_pathfile = os.path.join(attachment.path, attachment.name)
            new_pathfile = os.path.join(attachment.path, new_name)
            os.rename(old_pathfile, new_pathfile)
            _, path_file = new_pathfile.split('files')

            ftp_link = (path_ftp + path_file).replace(' ', '%20')
            attachment.write([attachment], {
                'name': new_name,
                'ftp_link': ftp_link,
            })

    def get_mail_users_target(self):
        emails = None
        for user in self.users_target:
            if user.email:
                if not emails:
                    emails = user.email
                else:
                    emails += ',' + user.email
        return emails

    @fields.depends('comm_group', 'users_target')
    def on_change_comm_group(self):
        if not self.comm_group:
            self.users_target = None
        else:
            self.users_target = [u.id for u in self.comm_group.users]

    @fields.depends('kind', 'party')
    def on_change_kind(self):
        if self.kind and self.kind.default_party:
            self.party = self.kind.default_party.id

    def get_date_communication_tz(self, name=None):
        date_not_utc = self.date_communication.replace(tzinfo=from_zone)
        date_not_utc = date_not_utc.astimezone(to_zone)
        date_not_utc = date_not_utc.strftime("%d/%b/%Y %I:%M%p")
        return date_not_utc


class CommunicationAttachment(ModelSQL, ModelView):
    'Communication Attachment'
    __name__ = 'document.communication.attachment'
    _rec_name = 'name'
    name = fields.Char('Name', required=True, states={'readonly': True})
    communication = fields.Many2One('document.communication',
        'Communication')
    data = fields.Function(fields.Binary('Data', filename='name'),
            'get_data', setter='set_data')
    path = fields.Char('Path', states={
            'required': False,
            'readonly': True,
        })
    description = fields.Char('Description')
    employee = fields.Many2One('company.employee', 'Employee',
            required=False)
    type = fields.Selection(_TYPE, 'Type', select=True, states={
            'readonly': True,
        })
    parent = fields.Many2One('document.communication.attachment', 'Parent',
            select=True, ondelete="RESTRICT")
    pages = fields.Integer('Pages', states={'readonly': True})
    indexed = fields.Boolean('Indexed', states={'readonly': True})
    date_document = fields.Date('Date Document')
    ftp_link = fields.Char('FTP Link', states={
            'required': False,
            'readonly': True,
    })
    file_link = fields.Function(fields.Char('File Link'), 'get_file_link')

    @classmethod
    def __setup__(cls):
        super(CommunicationAttachment, cls).__setup__()

    @staticmethod
    def default_type():
        return Transaction().context.get('type_communication')

    @classmethod
    def _check_directory_structure(cls):
        config = Pool().get('document.configuration')(1)
        if config.path_home is None:
            raise AccessError(
                gettext('document_communication.msg_missing_path_dms')
            )

        if not config.company_name_path:
            raise AccessError(
                gettext('document_communication.msg_missing_company_config')
            )

        base_dir = os.path.join(config.path_home, config.company_name_path)

        if not os.path.isdir(base_dir):
            os.makedirs(base_dir, 0755)

        path_dir_comm = os.path.join(base_dir, config.dir_comm)

        if not os.path.exists(path_dir_comm):
            os.mkdir(path_dir_comm, 0755)

        year = str(date.today().year)
        path_year = os.path.join(path_dir_comm, year)
        if not os.path.exists(path_year):
            os.mkdir(path_year, 0755)
        return path_year

    @classmethod
    def set_data(cls, attachments, name, value):
        if value is None or not attachments:
            return
        path_year = cls._check_directory_structure()

        for attachment in attachments:
            num_pages = None
            if attachment.communication:
                communication = attachment.communication
            elif attachment.parent and attachment.parent.communication:
                communication = attachment.parent.communication
            else:
                print "Warning: Not document fixed"
                continue

            path_kind = os.path.join(path_year, communication.kind.name)
            if not os.path.exists(path_kind):
                os.mkdir(path_kind, 0755)

            path_file = os.path.join(path_kind, attachment.name).encode('utf-8')

            # finally, write "output" with watermark to real file
            vals = attachment.set_watermark(value, communication)
            file_p = open(path_file, 'wb')
            vals['output'].write(file_p)
            file_p.close()
            num_pages = vals['num_pages']
        cls.write(attachments, {
            'path': path_kind,
            'pages': num_pages,
        })

    def get_file_link(self, name):
        config = Config.get_configuration()
        path_ftp = config.path_ftp
        if self.ftp_link:
            return path_ftp + self.ftp_link

    def get_data(self, name):
        value = None
        if self.path and self.name:
            try:
                pathfile = os.path.join(self.path, self.name).encode('utf-8')
                with open(pathfile, 'rb') as file_p:
                    value = fields.Binary.cast(file_p.read())
            except IOError as e:
                raise AccessError(
                    gettext('document_communication.msg_file_not_found', s=e)
                )
        return value

    def set_watermark(self, buff_file, communication):
        """
        Set reference watermark on document PDF and return a output writeable
        """

        ref = u'Radicado No. ' + communication.number
        date_ = 'Fecha: ' + communication.date_communication_tz
        if communication.comm_group:
            comm_group = communication.comm_group.name
        else:
            comm_group = ''
        comm_group = 'Dependencia: ' + comm_group

        appends = ''
        if self.communication.appends:
            appends = self.communication.appends[:20]
        folio = 'Folios: ' + appends

        if self.type in ['outgoing', 'response']:
            user = 'Envia: ' + self.create_uid.name
            comments = '--- Para respuesta citar Radicado ---'
        else:
            user = 'Recibe: ' + self.create_uid.name
            comments = '- Recibido para su estudio, no implica aceptacion -'

        # read existing PDF
        bytes_io = BytesIO(buff_file)
        reader = BufferedReader(bytes_io)
        try:
            existing_pdf = PdfFileReader(reader)
            page_zero = existing_pdf.getPage(0)
        except:
            raise AccessError(
                gettext('document_communication.msg_document_pdf_invalid')
            )
        x_dim = int(page_zero.mediaBox.getUpperRight_x())
        y_dim = int(page_zero.mediaBox.getUpperRight_y())

        # create a new PDF with Reportlab
        # Frame
        rect_length, rect_width = (165, 80)
        delta_text_y = 10
        yrect = 62
        y = 0
        ytext = 0
        ytext1 = 18
        ytext2 = ytext1 + delta_text_y
        ytext3 = ytext2 + delta_text_y
        ytext4 = ytext3 + delta_text_y
        ytext5 = ytext4 + delta_text_y
        ytext6 = ytext5 + delta_text_y
        ytext7 = ytext6 + delta_text_y
        if communication.tag_location == 'left':
            xtext = x_dim * 0.8
        elif communication.tag_location == 'center':
            xtext = x_dim * 0.5
        elif communication.tag_location == 'right':
            xtext = x_dim * 0.25
        elif communication.tag_location == 'below':
            xtext = x_dim * 0.25
            ytext = y_dim * 0.85
            y = y_dim * 0.85
        elif communication.tag_location == 'middle':
            xtext = x_dim * 0.35
            ytext = y_dim * 0.20
            y = y_dim * 0.20
        elif communication.tag_location == 'middle_b':
            xtext = x_dim * 0.35
            ytext = y_dim * 0.24
            y = y_dim * 0.24
        xtext = xtext + (rect_length * 0.5)
        xrect = xtext + 5
        yrect = 35 + 50

        string_io = BytesIO()
        can = canvas.Canvas(string_io, pagesize=letter)
        can.setLineWidth(0.5)
        can.setFillColor(HexColor(0xFCFCFF), alpha=0.65)
        can.setDash([3, 2, 1, 2], phase=0)
        can.setStrokeColor(HexColor(0xB3B7BA))
        can.rect((x_dim - xrect), (y_dim - yrect - y), rect_length, rect_width, fill=1)

        # Text Frame
        y_dim = y_dim - ytext
        x_dim = x_dim - xtext
        can.setFont("Helvetica-Oblique", 6)
        can.setFillColorRGB(0.2, 0.2, 0.2)
        can.drawString((x_dim), (y_dim - ytext1), communication.company.party.name)
        can.setFont("Helvetica-Bold", 6)
        can.drawString((x_dim), (y_dim - ytext2), ref)
        can.setFont("Helvetica", 6)
        can.drawString(x_dim, (y_dim - ytext3), date_)
        can.drawString(x_dim, (y_dim - ytext4), user)
        can.drawString(x_dim, (y_dim - ytext5), comm_group)
        can.drawString(x_dim, (y_dim - ytext6), folio)
        can.setFont("Helvetica-Bold", 6)
        can.drawString((x_dim + 8), (y_dim - ytext7), comments)

        can.showPage()
        can.save()

        #move to the beginning of the StringIO buffer
        string_io.seek(0)
        new_pdf = PdfFileReader(string_io)

        # add the "watermark" (which is the new pdf) on the existing page
        output = PdfFileWriter()
        num_pages = existing_pdf.getNumPages()
        for i in range(num_pages):
            page = existing_pdf.getPage(i)
            if i == 0:
                try:
                    page.mergePage(new_pdf.getPage(0))
                except:
                    raise AccessError(
                        gettext('document_communication.msg_document_pdf_invalid')
                    )
            output.addPage(page)
        values = {}
        values['output'] = output
        values['num_pages'] = num_pages
        return values


class AddAttachmentStart(ModelView):
    'Add Attachment Start'
    __name__ = 'document.communication.add_attachment.start'
    attachment = fields.Many2One('document.communication.attachment',
        'Attachment', required=True)
    name = fields.Char('Name Document', required=True)
    attach_file = fields.Binary('File', filename='name', required=True)
    date = fields.Date('Date', required=True)
    description = fields.Text('Description', required=False)

    @staticmethod
    def default_date():
        return date.today()


class AddAttachment(Wizard):
    'Add Attachment'
    __name__ = 'document.communication.add_attachment'
    start_state = 'select_attachment'
    select_attachment = StateTransition()
    start = StateView('document.communication.add_attachment.start',
        'document.communication_add_attachment_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add', 'search_', 'tryton-ok', default=True),
        ])
    search_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(AddAttachment, cls).__setup__()

    def transition_select_attachment(self):
        pool = Pool()
        Communication = pool.get('document.communication')
        AddAttachmentStart = pool.get('document.communication.add_attachment.start')
        communication = Communication(Transaction().context.get('active_id'))
        attachment_ids = [attachment.id for attachment in communication.attachments]
        if not communication.attachments:
            return 'end'
        dom = [
                ('id', 'in', attachment_ids),
        ]
        AddAttachmentStart.attachment.domain = dom
        return 'start'

    def transition_search_(self):
        pool = Pool()
        Communication = pool.get('document.communication')
        Attachment = pool.get('document.communication.attachment')

        communication = Communication(Transaction().context.get('active_id'))
        name_doc = 'RES_' + communication.number + '_' + self.start.name
        if not self.start.description:
            description = name_doc
        else:
            description = self.start.description

        for attachment in communication.attachments:
            if attachment.id != self.start.attachment.id:
                continue

            response = {
                'name': name_doc,
                'parent': self.start.attachment.id,
                'date_document': self.start.date,
                'data': self.start.attach_file,
                'description': description,
                'type': 'response',
            }
            Attachment.write([attachment], {
                'responses': [('create', [response])],
            })
        return 'end'


class DocumentCommunicationUser(ModelSQL):
    'Document Communication User'
    __name__ = 'res.user-document.communication'
    _table = 'res_user_document_communication'
    document_communication = fields.Many2One('document.communication',
        'Document Communication', ondelete='CASCADE', select=True,
        required=True)
    user = fields.Many2One('res.user', 'User', select=True,
        required=True, ondelete='RESTRICT')


class CommunicationLetter(Workflow, ModelSQL, ModelView):
    'Communication Letter'
    __name__ = 'document.communication.letter'
    date = fields.Date('Date', select=True, states=_STATES)
    number = fields.Char('Number', readonly=True, select=True)
    subject = fields.Char('Subject', required=True, states=_STATES)
    company = fields.Many2One('company.company', 'Company',
            states=_STATES, required=True)
    reference = fields.Many2One('document.communication.letter_reference',
            'Reference', states=_STATES)
    party = fields.Many2One('party.party', 'Party', required=True,
            select=True, states=_STATES)
    kind = fields.Many2One('document.communication.kind',
        'Communication Kind', required=True, states={
            'readonly': Bool(Eval('number')),
            }, domain=[('type', '=', 'outgoing')],
    )
    priority = fields.Selection(PRIORITY, 'Priority', required=True,
            select=True, states=_STATES)
    type = fields.Selection([
            ('legal', 'Legal'),
            ('party', 'Party'),
            ('client', 'Client'),
            ], 'Type', required=True, states=_STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirm', 'Confirm'),
            ('signed', 'Signed'),
            ('done', 'Done'),
            ('canceled', 'Canceled'),
            ], 'State', required=True, readonly=True)
    append = fields.Char('Append', states=_STATES)
    folios = fields.Char('Folios', states=_STATES)
    comm_group = fields.Many2One('document.communication.group', 'Group',
        states=_STATES, required=True)
    name = fields.Char('Name', select=True, states={
            'readonly': True,
        })
    file_report = fields.Function(fields.Binary('File Report', filename='name',
            states={
                'readonly': ~Eval('state').in_(['draft', 'confirm']),
                'required': Eval('state').in_(['signed', 'done']),
                }), 'get_data', setter='set_data')
    path = fields.Char('Path', states={
            'readonly': True,
    })
    communication_incoming = fields.Many2One('document.communication',
        'Communication Incoming', select=True, readonly=True)
    communication_outgoing = fields.Many2One('document.communication',
        'Communication Outgoing', select=True, readonly=True)

    @classmethod
    def __setup__(cls):
        super(CommunicationLetter, cls).__setup__()
        cls._transitions |= set((
                ('draft', 'confirm'),
                ('confirm', 'signed'),
                ('signed', 'done'),
                ('confirm', 'draft'),
                ('signed', 'canceled'),
                ('done', 'canceled'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') != 'confirm',
                    },
                'sign': {
                    'invisible': Eval('state') != 'confirm',
                    },
                'done': {
                    'invisible': Eval('state') != 'signed',
                    },
                'confirm': {
                    'invisible': Eval('state') != 'draft',
                    },
                'cancel': {
                    'invisible': Eval('state') != 'done',
                    },
                'edit': {
                    'invisible': Eval('state') != 'draft',
                    },
                })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_reference():
        Reference = Pool().get('document.communication.letter_reference')
        references = Reference.search([
            ('active', '=', True),
        ])
        if len(references) == 1:
            return references[0].id

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_priority():
        return 'middle'

    @fields.depends('kind', 'party')
    def on_change_kind(self):
        if self.kind and self.kind.default_party:
            self.party = self.kind.default_party.id

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, docs):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, docs):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, docs):
        for doc in docs:
            doc.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('signed')
    def sign(cls, docs):
        #TODO add digital signature
        for doc in docs:
            doc.update_name()

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, docs):
        for doc in docs:
            doc._create_communication()

    def set_number(self):
        if self.number:
            return
        Sequence = Pool().get('ir.sequence')
        if not self.kind or not self.kind.sequence:
            raise AccessError(
                gettext('document_communication.msg_sequence_missing')
            )
        seq = self.kind.sequence.get()
        self.write([self], {'number': seq})

    def _create_communication(self):
        Communication = Pool().get('document.communication')
        if self.communication_outgoing:
            return
        type_ = 'outgoing'
        if self.kind and self.kind.type:
            type_ = self.kind.type
        if not self.comm_group or not self.comm_group.users:
            raise AccessError(
                gettext('document_communication.msg_missing_target_users')
            )
        values = {
            'number': self.number,
            'party': self.party,
            'kind': self.kind,
            'description': self.subject,
            'tag_location': 'right',
            'state': 'draft',
            'type_communication': type_,
            'priority': self.priority,
            'company': self.company.id,
            'appends': self.folios,
            'users_target': [('add', self.comm_group.users)],
            'comm_group': self.comm_group.id,
        }
        communication, = Communication.create([values])
        self.write([self], {'communication_outgoing': communication})

    @classmethod
    def set_data(cls, letters, name, value):
        Attachment = Pool().get('document.communication.attachment')
        if value is None or not letters:
            return
        path_year = Attachment._check_directory_structure()

        for let in letters:
            path_letters = os.path.join(path_year, 'CARTAS')
            if not os.path.exists(path_letters):
                os.mkdir(path_letters.encode('utf-8'), 0755)
            path_kind = os.path.join(path_letters, let.kind.name)
            if not os.path.exists(path_kind):
                os.mkdir(path_kind.encode('utf-8'), 0755)
            if not let.name:
                return
            path_file = os.path.join(path_kind, let.name).encode('utf-8')

            with open(path_file, 'wb') as file_p:
                file_p.write(value)

            cls.write([let], {
                'path': path_kind,
            })

    def get_data(self, name):
        value = None
        if self.path and self.name:
            try:
                pathfile = os.path.join(self.path, self.name).encode('utf-8')
                print pathfile
                if os.path.exists(pathfile):
                    with open(pathfile, 'rb') as file_p:
                        value = fields.Binary.cast(file_p.read())
            except IOError as e:
                missing_target_users
                raise AccessError(
                    gettext('document_communication.msg_file_not_found', s=e)
                )
        return value

    def convert_pdf(self):
        # Warning method on deprecation
        pathfile = os.path.join(self.path, self.name)
        try:
            subprocess.call(['unoconv', '-f', 'pdf', pathfile])
            name_, ext = self.name.split('.')
            new_name = name_ + '.' + 'pdf'
            self.write([self], {'name': new_name})
            return True
        except:
            return False

    def update_name(self):
        pathfile = os.path.join(self.path, self.name)
        if self.name:
            try:
                name_, ext = self.name.split('.')
                if ext != 'pdf':
                    raise AccessError(
                        gettext('document_communication.msg_invalid_pdf_file')
                    )
                new_name = self.number + '_' + self.name
                new_pathfile = os.path.join(self.path, new_name).encode('utf-8')
                os.rename(pathfile, new_pathfile)
                self.write([self], {'name': new_name})
            except:
                raise AccessError(
                    gettext('document_communication.msg_invalid_pdf_file')
                )


class CommunicationLetterReport(CompanyReport):
    __name__ = 'document.communication.letter'


class CommunicationKind(ModelSQL, ModelView):
    'Communication Kind'
    __name__ = 'document.communication.kind'
    name = fields.Char('Name', required=True)
    sequence = fields.Many2One('ir.sequence', 'Communication Sequence',
        required=True, domain=[
            ('code', '=', 'document.communication')
        ])
    default_party = fields.Many2One('party.party', 'Default Party')
    type = fields.Selection(_TYPE, 'Type', select=True)


class LetterReference(ModelSQL, ModelView):
    'Communication Letter Reference'
    __name__ = 'document.communication.letter_reference'
    name = fields.Char('Name', required=True, select=True)
    active = fields.Boolean('Active')
    description = fields.Text('Description')

    @staticmethod
    def default_active():
        return True


class TagReport(Report):
    'Tag Report'
    __name__ = 'document.communication.tag_report'


class CommunicationFixNumberStart(ModelView):
    'Communication Fix Number Start'
    __name__ = 'document.communication_fix_number.start'
    number = fields.Char('New Number', required=True)


class CommunicationFixNumber(Wizard):
    'Communication Fix Number'
    __name__ = 'document.communication_fix_number'
    start = StateView('document.communication_fix_number.start',
        'document.communication_fix_number_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        cursor = Transaction().connection.cursor()
        ids = Transaction().context['active_ids']
        if ids:
            number = str(self.start.number)
            id_ = str(ids[0])
            query = "UPDATE document_communication SET number='%s' WHERE id=%s"
            cursor.execute(query % (number, id_))
        return 'end'


class CommunicationAddUserStart(ModelView):
    'Communication Add User Start'
    __name__ = 'document.communication_add_user.start'
    user = fields.Many2One('res.user', 'User', required=True)


class CommunicationAddUser(Wizard):
    'Communication Add User'
    __name__ = 'document.communication_add_user'
    start = StateView('document.communication_add_user.start',
        'document_communication.communication_add_user_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        ids = Transaction().context['active_ids']
        Communication = Pool().get('document.communication')
        if ids:
            comm = Communication(ids[0])
            Communication.write([comm], {
                'users_target': [('add', [self.start.user.id])],
                }
            )
        return 'end'


class CommunicationSummaryStart(ModelView):
    'Communication Summary Start'
    __name__ = 'document.communication_summary.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    type = fields.Selection(_TYPE, 'Type')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()


class CommunicationSummary(Wizard):
    'Communication Summary'
    __name__ = 'document.communication_summary'
    start = StateView('document.communication_summary.start',
        'document_communication.communication_summary_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('document.communication_summary.report')

    def do_print_(self, action):
        type_ = None
        if self.start.type:
            type_ = self.start.type
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'type': type_,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class CommunicationSummaryReport(Report):
    'Communication Summary Report'
    __name__ = 'document.communication_summary.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Communication = pool.get('document.communication')
        Company = pool.get('company.company')
        time_start = time(0, 0, 0)
        time_end = time(23, 59, 0)
        start_date_ts = datetime.combine(data['start_date'], time_start)
        end_date_ts = datetime.combine(data['end_date'], time_end)
        dom_comm = [
            ('date_communication', '>=', start_date_ts),
            ('date_communication', '<=', end_date_ts),
        ]
        if data['type']:
            dom_comm.append(
                ('type_communication', '=', data['type'])
            )

        report_context['records'] = Communication.search(dom_comm)
        report_context['type_'] = data['type']
        report_context['company'] = Company(data['company']).party.name
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        return report_context


class CommunicationForceDraft(Wizard):
    'Communication Force Draft'
    __name__ = 'document.communication.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        cursor = Transaction().connection.cursor()
        ids = Transaction().context['active_ids']
        id_communication = str(ids[0])
        query = "UPDATE document_communication SET state='draft' WHERE id=%s"
        cursor.execute(query % id_communication)
        return 'end'


class CommunicationLetterForceDraft(Wizard):
    'Communication Letter Force Draft'
    __name__ = 'document.communication_letter.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        cursor = Transaction().connection.cursor()
        ids = Transaction().context['active_ids']
        if ids:
            id_record = str(ids[0])
            query = "UPDATE document_communication_letter SET state='draft' WHERE id=%s"
            cursor.execute(query % id_record)
        return 'end'
